//
// Created by nikita on 19.01.2022.
//

#include "HumanData.h"



HumanData::HumanData() {

}

void HumanData::setAddress(const string &address) {
    HumanData::address = address;
}

void HumanData::setPhone(const string &phone) {
    HumanData::phone = phone;
}

HumanData::HumanData(string addr, string ph) {
    this->address = addr;
    this->phone = ph;
}

istream &operator>>(istream &in, HumanData &data) {
    getline(in, data.address);
    getline(in, data.phone);
    return in;
}

ostream &operator<<(ostream &out, const HumanData &data) {
    out <<"телефон: "<<data.phone <<", адрес: " <<data.address <<endl;
    return out;
}
