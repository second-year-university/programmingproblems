//
// Created by nikita on 19.01.2022.
//

#ifndef SEARCHES_HUMANDATA_H
#define SEARCHES_HUMANDATA_H

#include <string>
#include <iostream>

using namespace std;

class HumanData {
    string address;
    string phone;

public:
    HumanData(string addr, string ph);
    HumanData();

    friend istream& operator >>(istream& in, HumanData& data);
    friend ostream& operator <<(ostream& out, const HumanData& data);

    void setAddress(const string &address);

    void setPhone(const string &phone);

};


#endif //SEARCHES_HUMANDATA_H
