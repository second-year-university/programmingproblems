//
// Created by nikita on 19.01.2022.
//

#ifndef SEARCHES_HUMAN_H
#define SEARCHES_HUMAN_H

#include <string>
#include <vector>
#include <fstream>
#include "HumanData.h"

using namespace std;

class Human {
private:
    HumanData data;
    string name;
    int hash;
    void calcHash();
public:
    Human();
    Human(string aname, string addr, string ph);

    Human(const string &name, const HumanData &data);

    friend istream& operator >>(istream& in, Human& human);
    friend ostream& operator <<(ostream& out, const Human& human);

    const string &getName() const;

    const HumanData &getData() const;

    bool operator ==(const Human& human) const;

    void setName(const string &name);

    int getHash() const;

    bool operator<(const Human &rhs) const;

    bool operator>(const Human &rhs) const;

    bool operator<=(const Human &rhs) const;

    bool operator>=(const Human &rhs) const;
};

#endif //SEARCHES_HUMAN_H
