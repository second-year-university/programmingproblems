//
// Created by nikita on 19.01.2022.
//

#include "HumanList.h"

HumanList::HumanList() {
    container.resize(100000);
}

HumanList::HumanList(string filename) {
    container.resize(100000);
    readHumans(filename);
    sort(list.begin(), list.end());
}

bool HumanList::readHumans(string filename) {
    ifstream in(filename);
    if (!in.is_open()){
        return false;
    }
    Human human;
    in >>human;
    while (!in.fail()){
        list.push_back(human);
        map_list[human.getName()] = human.getData();
        container[human.getHash()].push_back(human);
        in >>human;
    }
    return true;
}

Human HumanList::findByName(const string &name) {
    for (auto it = list.begin(); it != list.end(); it++){
        if (it->getName() == name){
            return (*it);
        }
    }
    throw FindHumanException();
}

Human HumanList::findByName_binsearch(const string &name) {
    return findByName_binsearch(name, 0, list.size());
}

Human HumanList::findByName_binsearch(const string &name, int min, int max) {
    int index = (min+max)/2;
    if (min > max){
        throw FindHumanException();
    }
    else if (list[index].getName()==name){
        return list[index];
    }
    else if (list[index].getName() > name){
        return findByName_binsearch(name, min, index - 1);
    }
    else{
        return findByName_binsearch(name, index+1, max);
    }
}

Human HumanList::findByName_map(const string &name) {
    auto it = map_list.find(name);
    if (it == map_list.end()){
        throw FindHumanException();
    }
    else{
        return Human(it->first, it->second);
    }
}

Human HumanList::findByName_hash(const string &name) {
    Human toFind(name, "", "");
    for (auto it = list.begin(); it != list.end(); it++){
        if ((*it)==toFind){
            return (*it);
        }
    }
    throw FindHumanException();
}

Human HumanList::findByName_binsearch_hash(const string &name, int min, int max) {
    int index = (min+max)/2;
    Human toFind(name, "", "");
    if (min > max){
        throw FindHumanException();
    }
    else if (list[index]==toFind){
        return list[index];
    }
    else if (list[index] > toFind){
        return findByName_binsearch(name, min, index - 1);
    }
    else{
        return findByName_binsearch(name, index+1, max);
    }
}

Human HumanList::findByName_binsearch_hash(const string &name) {
    return findByName_binsearch_hash(name, 0, list.size());
}

Human HumanList::findByName_hash_real(const string &name) {
    Human toFind(name, "", "");
    for (int i = 0; i < container[toFind.getHash()].size(); i++){
        if (toFind == container[toFind.getHash()][i]){
            return container[toFind.getHash()][i];
        }
    }
    throw FindHumanException();
}
