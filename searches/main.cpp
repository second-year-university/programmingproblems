#include <iostream>
#include "HumanList.h"

int main() {
    HumanList humanList("humans.txt");
    cout<<humanList.findByName("Алексей");
    //cout <<humanList.findByName_binsearch("Алексей");
    cout <<humanList.findByName_map("Александр");
    cout <<humanList.findByName_hash("Пётр");
    cout <<humanList.findByName_binsearch_hash("Василий");
    cout <<humanList.findByName_hash_real("Антонио");
}

