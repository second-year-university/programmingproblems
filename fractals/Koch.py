import turtle


def Koch(length):
    if length <= 2:
        turtle.fd(length)
        return
    Koch(length / 3)
    turtle.lt(60)
    Koch(length / 3)
    turtle.rt(120)
    Koch(length / 3)
    turtle.lt(60)
    Koch(length / 3)


length = float(input('Введите длину: '))
turtle.speed(0)
turtle.penup()
turtle.backward(length / 2.0)
turtle.pendown()
Koch(length)
turtle.done()
