//
// Created by nikita on 20.01.2022.
//

#include "Quadruple.h"


Quadruple::Quadruple(string number) {
    bool beforeDec = true;
    bool signMinus = false;
    string leftPart = "", rightPart = "";
    if (number[0] == '-'){
        signMinus = true;
    }
    for (int i = 0; i < number.length(); i++){
        if (i == 0 && number[i] == '-'){
            continue;
        }
        if (number[i] == '.'){
            beforeDec = false;
            continue;
        }
        if (beforeDec){
            leftPart += number[i];
        }
        else{
            rightPart += number[i];
        }
    }
    QudraLong qLeft(leftPart);
    int leftBits = qLeft.bits_used();

    QudraLong qRight(rightPart);
    if (qRight.getSenior() != 0){
        throw QudrupleException();
    }
    unsigned long long ullRight = qRight.getJunior();
    unsigned long long denom = 1;
    while (denom < ullRight){
        denom*=10;
    }
    Rational rRight(ullRight, denom), sumRight(0);
    unsigned long long binaryRight = 0;
    unsigned long long  num = 1;
    int zeroes = 0;
    bool zero = true;
    for (int i = 0; i < 64; i++){
        if (rRight >= sumRight + Rational(1, num)){
            sumRight += Rational(1, num);
            binaryRight = binaryRight | 1;
            zero = false;
        }
        if (zero){
            zeroes++;
        }
        binaryRight = binaryRight << 1;
        num*=2;
    }
    //cout <<binaryRight;

    int exponent = 0;
    if (leftBits > 0){
        exponent = leftBits - 1;
    }
    else{
        exponent = -zeroes - 1;
    }
    this->setMinus(signMinus);

    int readyExponent = 0;

    if (exponent >= 0){
        readyExponent = abs(abs(exponent) - ((1<<14) - 1));
    }
    else{
        readyExponent = abs(-abs(exponent) - ((1<<14) - 1));
    }

    this->setMatis(readyExponent);
    QudraLong qRRight;
    qRRight.setSenior(binaryRight);

    //cout <<leftBits <<endl;
    (*this) = (*this) | ((qLeft << (129 - leftBits))>>16);
    (*this) = (*this) | (qRRight >> (15 + leftBits));
    //cout <<(*this) <<endl;
}

ostream &operator<<(ostream &os, const Quadruple &quadruple) {
    os <<hex <<quadruple.senior <<" " <<quadruple.junior;
}

Quadruple Quadruple::operator*(const Quadruple &r) const {
    Quadruple q("0.0");
    int newMatis = getMatis() + r.getMatis();
    q.setMatis(newMatis);
    //TODO
    return Quadruple("0.0");
}

Quadruple Quadruple::operator/(const Quadruple &r) const {
    Quadruple q("0.0");
    int newMatis = getMatis() - r.getMatis();
    q.setMatis(newMatis);
    //TODO
    return Quadruple("0.0");
}

Quadruple Quadruple::pow(int d) {
    Quadruple q("1.0");
    for (int i = 0; i < d; i++){
        q = q * (*this);
    }
    return  q;
}

int Quadruple::getMatis() const {
    return (senior>>48)&(0b111111111111111);
}

void Quadruple::setMatis(unsigned int matis) {
    //cout <<hex <<((matis + (1<<14) - 1)&0x7FFF) <<endl;
    unsigned long long ullMatis = matis&0x7FFF;
    senior = senior | ((ullMatis)<<48);
}

bool Quadruple::isMinus() const {
    return senior>>63;
}

void Quadruple::setMinus(bool minus) {
   if (minus){
       senior = senior | (1ULL<<63);
   }
}

Quadruple Quadruple::operator&(const QudraLong &r) const {
    Quadruple q;
    q.senior = senior & r.getSenior();
    q.junior = junior & r.getJunior();
    return q;
}

Quadruple Quadruple::operator|(const QudraLong &r) const {
    Quadruple q;
    q.senior = senior | r.getSenior();
    q.junior = junior | r.getJunior();
    return q;
}

Quadruple Quadruple::operator=(const Quadruple &r) {
    senior = r.senior;
    junior = r.junior;
}

Quadruple::Quadruple() {
    senior = 0;
    junior = 0;
}
