//
// Created by nikita on 20.01.2022.
//

#include "QudraLong.h"

QudraLong::QudraLong(string num) {
    int degree = 1;
    for (int i = num.length() - 1; i >= 0; i--){
        unsigned  long long before = junior;
        junior += QundraLongConstants::juniors[degree - 1][num[i]-'0'];
        if (junior <= before && num[i] != '0'){
            senior++;
        }
        senior+=QundraLongConstants::seniors[degree - 1][num[i]-'0'];
        degree++;
    }
}

unsigned long long int QudraLong::getSenior() const {
    return senior;
}

unsigned long long int QudraLong::getJunior() const {
    return junior;
}

int QudraLong::bits_used() {
    bool seniorNull = false;
    unsigned long long toCheck = senior;
    if (senior == 0){
        seniorNull = true;
        toCheck = junior;
    }
    int degree = 0;
    unsigned long long num = 1;
    while (num <= toCheck){
        num *= 2;
        degree++;
    }
    if (!seniorNull){
        degree += 64;
    }
    return degree;
}

bool QudraLong::operator<(const QudraLong &rhs) const {
    if (senior < rhs.senior){
        return true;
    }
    else if (senior == rhs.senior){
        return junior < rhs.junior;
    }
    else{
        return false;
    }
}

bool QudraLong::operator>(const QudraLong &rhs) const {
    return rhs < *this;
}

bool QudraLong::operator<=(const QudraLong &rhs) const {
    return !(rhs < *this);
}

bool QudraLong::operator>=(const QudraLong &rhs) const {
    return !(*this < rhs);
}

bool QudraLong::operator==(const QudraLong &rhs) const {
    return senior == rhs.senior &&
           junior == rhs.junior;
}

bool QudraLong::operator!=(const QudraLong &rhs) const {
    return !(rhs == *this);
}

QudraLong QudraLong::operator+(const QudraLong &r) const {
    QudraLong q("0");
    q.junior = junior + r.junior;
    if ((q.junior <= r.junior || q.junior <= junior) && r.junior != 0 && junior != 0){
        q.senior++;
    }
    q.senior += senior + r.senior;
    return q;
}

QudraLong QudraLong::operator<<(int n)  const{
    QudraLong q;
    if (n < 64){
        q.senior = senior <<n;
        q.senior = q.senior | (junior >> (64 - n));
        q.junior = junior << n;
        return q;
    }
    else{
        q.senior = junior << (n - 64);
        q.junior = 0;
        return q;
    }
}

QudraLong QudraLong::operator>>(int n) const {
    QudraLong q;
    if (n < 64){
        q.junior = junior >>n;
        q.junior = q.junior | (senior << (64 - n));
        q.senior = senior >>n;
        return q;
    }
    else{
        q.junior = senior >> (n - 64);
        q.senior = 0;
        return q;
    }
}

ostream &operator<<(ostream &os, const QudraLong &aLong) {
    os.flags (ios::hex | ios::showbase );
    os <<hex <<aLong.senior <<" " <<aLong.junior;
    return os;
}

void QudraLong::setSenior(unsigned long long int senior) {
    QudraLong::senior = senior;
}

void QudraLong::setJunior(unsigned long long int junior) {
    QudraLong::junior = junior;
}

QudraLong::QudraLong() {
    junior = 0;
    senior = 0;
}
