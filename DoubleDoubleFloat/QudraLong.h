//
// Created by nikita on 20.01.2022.
//

#ifndef DOUBLEDOUBLEFLOAT_QUDRALONG_H
#define DOUBLEDOUBLEFLOAT_QUDRALONG_H

#include <string>
#include <ostream>
#include "QundraLongConstants.h"

using namespace std;

class QudraLongException{};

class QudraLong {
    unsigned long long senior = 0, junior = 0;
public:
    QudraLong(string num);
    QudraLong();

    void initVars();

    unsigned long long int getSenior() const;

    unsigned long long int getJunior() const;

    int bits_used();

    bool operator<(const QudraLong &rhs) const;

    bool operator>(const QudraLong &rhs) const;

    bool operator<=(const QudraLong &rhs) const;

    bool operator>=(const QudraLong &rhs) const;

    bool operator==(const QudraLong &rhs) const;

    bool operator!=(const QudraLong &rhs) const;

    QudraLong operator +(const QudraLong& r) const;

    QudraLong operator <<(int n) const;
    QudraLong operator >>(int n) const;

    friend ostream &operator<<(ostream &os, const QudraLong &aLong);

    void setSenior(unsigned long long int senior);

    void setJunior(unsigned long long int junior);
};


#endif //DOUBLEDOUBLEFLOAT_QUDRALONG_H
