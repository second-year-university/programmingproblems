cmake_minimum_required(VERSION 3.20)
project(myVector)

set(CMAKE_CXX_STANDARD 20)

add_executable(myVector main.cpp newVector.h newVector.cpp)
