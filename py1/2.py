num = int(input())
ch = 1

while ch < num:
    ch *= 2

if ch == num:
    print("YES")
else:
    print("NO")
