import numpy
from matplotlib import pyplot as plt
import math

f = open('line-1.dat')

f.readline()

x = []
y = []
xy = []
x2 = []
y2 = []

for line in f:
    data = list(map(float, line.split()))
    x.append(data[0])
    y.append(data[1])
    xy.append(x[-1] * y[-1])
    x2.append(x[-1] ** 2)

n = len(x)

inversed = numpy.linalg.inv([[sum(x2), sum(x)], [sum(x), n]])
result = numpy.dot(inversed, [[sum(xy)], [sum(y)]])

k = result[0][0]
b = result[1][0]

print("y = %sx + %s" % (k, b))

for xi in x:
    y2.append(xi * k + b)

plt.plot(x, y)
plt.plot(x, y2)
plt.show()

print(math.degrees(math.atan(k)))
print("%s * %s + %s = %s" % (k, 30, b, k * 30 + b))