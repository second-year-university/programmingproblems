import numpy
from matplotlib import pyplot as plt
import math

f = open('giperpola-1.dat')

f.readline()

x = []
y = []
y2 = []
x2 = []
x3 = []
x4 = []
x5 = []
x6 = []
x3y = []
x2y = []
xy = []


for line in f:
    data = list(map(float, line.split()))
    x.append(data[0])
    y.append(data[1])
    x2.append(x[-1]**2)
    x3.append(x[-1]**3)
    x4.append(x[-1]**4)
    x5.append(x[-1]**5)
    x6.append(x[-1]**6)
    xy.append(x[-1] * y[-1])
    x2y.append(x2[-1] * y[-1])
    x3y.append(x3[-1] * y[-1])

n = len(x)

inversed = numpy.linalg.inv([
    [sum(x6), sum(x5), sum(x4), sum(x3)],
    [sum(x5), sum(x4), sum(x3), sum(x2)],
    [sum(x4), sum(x3), sum(x2), sum(x)],
    [sum(x3), sum(x2), sum(x), n]
])

result = numpy.dot(inversed, [[sum(x3y)], [sum(x2y)], [sum(xy)], [sum(y)]])
a, b, c, d = result

for xi in x:
    y2.append(a*(xi**3) + b * (xi**2) + c*xi + d)

roots = numpy.roots([a[0], b[0], c[0], d[0]])
print(roots)
plt.plot(x, y, "-r", label='Из файла')
plt.plot(x, y2, "--b", label='Восстановленная')
plt.plot(roots, [0, 0, 0], "-g", label='Корни')
plt.grid()
plt.legend()
plt.show()

print(a, b, c, d)
