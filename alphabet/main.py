import os

alphabet = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с',
            'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', ' ', '.', ',', '?', '!', ':',
            'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С',
            'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

books = os.listdir('books')

step = 3


def generate_combo(l, depth, ga):
    if depth == 0:
        ga.append(l)
        return
    depth -= 1
    for letter in alphabet:
        generate_combo(l + letter, depth, ga)


generated_alphabet = []
generate_combo('', step, generated_alphabet)

result = {}

for word in generated_alphabet:
    result[word] = 0


for book in books:
    f = open('books/' + book)
    text = f.read()
    for i in range(len(text)):
        print(int(i / len(text) * 100))
        word = text[i:i + step]
        if word in generated_alphabet:
            result[word] += 1
    f.close()

result_file = open('results/result' + str(step) + '.txt', 'w')

keys = result.keys()
for letter in result.keys():
    result_file.write(letter + ": " + str(result[letter]) + '\n')
