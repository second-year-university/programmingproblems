from matplotlib import pyplot as plt

dices = 11

chance = []
for i in range(dices):
    chance.append([0] * (dices * 6 + 1))
x = [i for i in range(dices * 6 + 1)]


def gen_cycle(d, depth, chances):
    for i in range(1, 7):
        if depth == 1:
            chances[d + i] += 1
        else:
            gen_cycle(d + i, depth - 1, chances)


for i in range(dices):
    gen_cycle(0, i + 1, chance[i])

for j in range(dices):
    for i in range(dices * 6 + 1):
        chance[j][i] /= (6 ** (j + 1))

for i in range(dices):
    plt.plot(x, chance[i])

legends = []

for i in range(dices):
    legends.append(str(i + 1) + ' кубика')
plt.legend(legends)
plt.title("Шансы выпадении чисел при броске %s кубиков" % dices)
plt.show()

result = [0] * (dices * 6 + 1)

beginners = [9999999999] * (dices + 1)
enders = [0] * (dices + 1)


for i in range(len(chance[0])):
    who_is_max = 0
    how_is_max = 0
    for j in range(dices):
        if how_is_max < chance[j][i]:
            how_is_max = chance[j][i]
            who_is_max = j
    result[i] = who_is_max + 1

for i in range(1, len(result)):
    beginners[result[i]] = min(beginners[result[i]], i)
    enders[result[i]] = max(enders[result[i]], i)

for i in range(1, len(beginners)):
    print('От %s до %s очков будет %s кубиков' % (beginners[i], enders[i], i))
