import math
from matplotlib import pyplot as plt


def H(p):
    sum = 0
    for pi in p:
        sum += -pi*math.log(pi, 2)
    return sum


#x = [i/100 for i in range(1, 100)]
#y = [H(x[i]) for i in range(len(x))]

#plt.plot(x, y)
#plt.show()

list1 = [10/20, 5/20, 5/20]
list2 = [8/20, 8/20, 4/20]

print("First urn:", H(list1))
print("Second urn:", H(list2))
