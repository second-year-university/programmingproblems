import os
import sys

from matplotlib import pyplot as plt
import numpy as np
import my_shape

sys.setrecursionlimit(40000)

color_storage = 1

inputs_folder = 'tests/inputs'

def have_space(shape, data):
    for i in range(shape.y_min, shape.y_max + 1):
        nulls = False
        ones = False
        for j in range(shape.x_min, shape.x_max + 1):
            if data[i][j] != 0 and not nulls:
                ones = True
            if data[i][j] == 0 and ones:
                nulls = True
            if data[i][j] != 0 and ones and nulls:
                return True
    for i in range(shape.x_min, shape.x_max + 1):
        nulls = False
        ones = False
        for j in range(shape.y_min, shape.y_max + 1):
            if data[j][i] != 0 and not nulls:
                ones = True
            if data[j][i] == 0 and ones:
                nulls = True
            if data[j][i] != 0 and ones and nulls:
                return True

    return False


def set_color(i, j, data, color_to_find, color_to_set):
    if i < 0 or i >= len(data[0]):
        return
    if j < 0 or j >= len(data):
        return

    if data[i][j] != color_to_set:
        return

    data[i][j] = color_to_set

    set_color(i - 1, j - 1, data, color_to_find, color_to_set)
    set_color(i - 1, j, data, color_to_find, color_to_set)
    set_color(i - 1, j + 1, data, color_to_find, color_to_set)
    set_color(i, j - 1, data, color_to_find, color_to_set)
    set_color(i, j + 1, data, color_to_find, color_to_set)
    set_color(i + 1, j - 1, data, color_to_find, color_to_set)
    set_color(i + 1, j, data, color_to_find, color_to_set)
    set_color(i + 1, j + 1, data, color_to_find, color_to_set)


def colorise(i, j, data, color, shapes):
    global color_storage
    if i < 0 or i >= len(data[0]):
        return
    if j < 0 or j >= len(data):
        return

    if data[i][j] != 1:
        return

    if color == -1:
        color_storage += 1
        color = color_storage
        shapes.append(my_shape.MyShape(color))

    data[i][j] = color
    shapes[color].square += 1
    shapes[color].update_bounds(i, j)

    colorise(i - 1, j - 1, data, color, shapes)
    colorise(i - 1, j, data, color, shapes)
    colorise(i - 1, j + 1, data, color, shapes)
    colorise(i, j - 1, data, color, shapes)
    colorise(i, j + 1, data, color, shapes)
    colorise(i + 1, j - 1, data, color, shapes)
    colorise(i + 1, j, data, color, shapes)
    colorise(i + 1, j + 1, data, color, shapes)


def do_job(data):
    global color_storage
    shapes = [None, None]
    color_storage = 1
    for i in range(len(data[0])):
        for j in range(len(data)):
            if data[i][j] != 1:
                continue
            colorise(i, j, data, -1, shapes)

    rects = 0
    circles = 0
    triangles = 0
    noise = 0
    NA = 0
    for shape in shapes:
        if shape is None:
            continue
        shape.count_ratio()

        if shape.ratio > 0.9:
            shape.shape_type = 'rect'
            rects += 1
        elif shape.ratio <= 0.15:
            shape.shape_type = 'karakulya'
            noise += 1
        elif 0.15 < shape.ratio < 0.65:
            if have_space(shape, data):
                shape.shape_type = 'karakulya'
                noise += 1
            else:
                shape.shape_type = 'triangle'
                triangles += 1
        elif 0.7 < shape.ratio < 0.9:
            shape.shape_type = 'circle'
            circles += 1
        else:
            shape.shape_type = 'NA'
            NA += 1
        #print(shape.id, shape.ratio, shape.shape_type)
        norm_shape = data[shape.y_min:shape.y_max]
        another_shape = []
        for row in norm_shape:
            another_shape.append(row[shape.x_min:shape.x_max])
        #plt.imshow(another_shape)
        #plt.title(shape.shape_type + ' ' + str(shape.id) + ' ' + str(shape.ratio))
        #plt.show()
    print('Rectangle =', rects)
    print('Circle =', circles)
    print('Triangle =', triangles)
    print('Noise =', noise)
    print('NA =', NA)
    print('=============================\n')
    #plt.imshow(data)
    #plt.show()




files = os.listdir(inputs_folder)

for file in files:
    print(file)
    f = open(inputs_folder + '/' + file)
    ddata = []
    for line in f:
        line_list = []
        for sym in line:
            if sym not in ('0', '1'):
                continue
            line_list.append(int(sym))
        ddata.append(line_list)
    # print(ddata)
    f.close()
    do_job(ddata)
