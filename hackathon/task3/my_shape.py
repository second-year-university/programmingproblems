class MyShape:
    id = 0
    x_min = 99999999999
    x_max = -1
    y_min = 99999999999
    y_max = -1
    square = 0
    rect_square = 0
    shape_type = ''

    ratio = 0

    def __init__(self, id):
        self.id = id
        pass

    def update_bounds(self, y, x):
        self.x_max = max(x, self.x_max)
        self.y_max = max(y, self.y_max)
        self.x_min = min(x, self.x_min)
        self.y_min = min(y, self.y_min)

    def count_ratio(self):
        self.rect_square = (self.x_max - self.x_min) * (self.y_max - self.y_min)
        self.ratio = self.square / self.rect_square
