//
// Created by nikita on 11.11.2021.
//

#ifndef GRAPH_GRAPH_H
#define GRAPH_GRAPH_H

#include <string>
#include <set>
#include "Node.h"

using namespace std;

typedef set<Node*>::const_iterator node_iterator;

class Graph {
    set<Node*> nodes;
public:
    Graph(string fileName);
    void addNode(Node* node);
    void removeNode(Node* node);
    void addEdge(Node* begin, Node* end);
    void removeEdge(Node* begin, Node* end);
    node_iterator begin() const {
        return nodes.begin();
    }
    node_iterator end() const {
        return nodes.end();
    }
};



#endif //GRAPH_GRAPH_H
